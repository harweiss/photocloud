package com.linkesoft.photocloud;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.linkesoft.photocloud.ThumbnailsFragment.ThumbnailsFragmentInteractionListener;
import com.linkesoft.photocloud.databinding.ThumbnailBinding;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link File} and makes a call to the
 * specified {@link ThumbnailsFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class ThumbnailsRecyclerViewAdapter extends RecyclerView.Adapter<ThumbnailsRecyclerViewAdapter.ViewHolder> {

    private final List<File> photoFiles = new ArrayList<>();
    private final ThumbnailsFragmentInteractionListener listener;
    public int colunnWidth = 100;

    public ThumbnailsRecyclerViewAdapter(ThumbnailsFragmentInteractionListener listener) {
        this.listener = listener;
        refresh();
    }

    public void refresh() {
        photoFiles.clear();
        File[] files = MainActivity.localPhotoDir.listFiles();
        if(files == null)
            return;
        // sortiere nach Datum, absteigend
        Arrays.sort(files, new Comparator<File>() {
            @Override
            public int compare(File f1, File f2) {
                Long t1 = f1.lastModified();
                Long t2 = f2.lastModified();
                return t2.compareTo(t1);
            }
        });
        photoFiles.addAll(Arrays.asList(files));
        notifyDataSetChanged();
    }

    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.thumbnail, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        File file = photoFiles.get(position);
        holder.update(file);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != listener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    listener.onThumbnailSelected(holder.file);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return photoFiles.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        final ThumbnailBinding binding;
        File file;

        public ViewHolder(View view) {
            super(view);
            binding = ThumbnailBinding.bind(view);


        }

        void update(File file) {

            ViewGroup.LayoutParams layoutParams = itemView.getLayoutParams();
            layoutParams.width = colunnWidth;
            layoutParams.height = colunnWidth; // square
            itemView.setLayoutParams(layoutParams);

            this.file = file;
            Date fileDate = new Date(file.lastModified());
            String title = DateFormat.getDateTimeInstance(DateFormat.SHORT,DateFormat.SHORT).format(fileDate);
            binding.title.setText(title);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 8; // oder per inJustDecodeBounds aus der echten Bildgröße berechnen
            Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(),options);
            binding.image.setImageBitmap(bitmap);
            // upload indicator
            binding.checkbox.setVisibility(MainActivity.isUploaded(file) ? View.VISIBLE : View.GONE);
        }

        @NotNull
        @Override
        public String toString() {
            return super.toString() + " '" + file.getName() + "'";
        }
    }

    private static int dp(Context context, int px) {
        return (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                px,
                context.getResources().getDisplayMetrics());
    }
}
