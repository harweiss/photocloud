package com.linkesoft.photocloud;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.owncloud.android.lib.common.OwnCloudClient;
import com.owncloud.android.lib.common.OwnCloudClientFactory;
import com.owncloud.android.lib.common.OwnCloudCredentialsFactory;

import org.apache.commons.httpclient.methods.DeleteMethod;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

public class NextcloudLogin {
    private static final String nextCloudServer = "nextCloudServer";
    private static final String nextCloudUser = "nextCloudUser";
    private static final String nextCloudPassword = "nextCloudPassword";

    public static OwnCloudClient getClient(Context context) {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String baseURL = prefs.getString(nextCloudServer, null);
        if(baseURL==null)
            return null;
        String user = prefs.getString(nextCloudUser, null);
        if(user==null)
            return null;
        String password = prefs.getString(nextCloudPassword, null);
        if(password==null)
            return null;
        Uri serverUri = Uri.parse(baseURL);
        OwnCloudClient client = OwnCloudClientFactory.createOwnCloudClient(serverUri, context, true);
        client.setCredentials(
                OwnCloudCredentialsFactory.newBasicCredentials(
                        user,
                        password
                ));
        return client;
    }

    private static void save(Context context, String server, String user, String password) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefs.edit().putString(nextCloudServer, server).putString(nextCloudUser, user).putString(nextCloudPassword, password).apply();
    }
    public interface LoginHandler {
        void loginSuccess();
    }

    public static void login(final Context context, final NextcloudLogin.LoginHandler loginHandler) {
        // ask for server
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.VERTICAL);
        layout.setPadding(dp(context,10),dp(context,5),dp(context,10),dp(context,5));
        final EditText serverEditText = new EditText(context);
        serverEditText.setHint(R.string.Server);
        serverEditText.setText(prefs.getString(nextCloudServer, null));
        serverEditText.setInputType(EditorInfo.TYPE_TEXT_VARIATION_URI);
        layout.addView(serverEditText);
        final AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setView(layout).setTitle(R.string.NextcloudLogin).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                final String server = serverEditText.getText().toString();
                if(TextUtils.isEmpty(server)) {
                    serverEditText.requestFocus();
                } else {
                    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
                    prefs.edit().putString(nextCloudServer, server).apply();
                    loginFlow(context,server,loginHandler);
                }
            }
        });
        final AlertDialog dialog = alert.create();
        dialog.show();
    }
    @SuppressLint("SetJavaScriptEnabled")
    private static void loginFlow(final Context context, String server, final NextcloudLogin.LoginHandler loginHandler) {
        final AlertDialog.Builder alert = new AlertDialog.Builder(context);
        WebView webView = new WebView(context);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        String deviceName = Build.MODEL;
        webSettings.setUserAgentString(context.getString(R.string.app_name) + " " + deviceName);

        Map<String, String> additionalHttpHeaders = new HashMap<>();
        additionalHttpHeaders.put("OCS-APIREQUEST","true");
        webView.loadUrl( server + "/index.php/login/flow", additionalHttpHeaders);
        LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.VERTICAL);
        EditText dummyEditText = new EditText(context); // nötig, weil die Tastatur manchmal nicht korrekt angezeigt wird
        dummyEditText.setVisibility(View.GONE);
        layout.addView(webView, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        layout.addView(dummyEditText, LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        alert.setView(layout);
        final AlertDialog dialog = alert.create();
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view,
                                                    String url) {
                Uri uri = Uri.parse(url);
                if (uri != null && uri.getScheme() != null && uri.getScheme().equals("nc")) {
                    if (extractLoginInfo(uri, context)) {
                        loginHandler.loginSuccess();
                        dialog.dismiss();
                    }
                    return true;
                } else {
                    return false;
                }
            }
        });
        dialog.show();
    }

    private static boolean extractLoginInfo(Uri url, Context context) {
        // extract server, user, password from
        // nc://login/server:<server>&user:<loginname>&password:<password>
        String strurl = url.toString();
        String prefix = "nc://login/";
        if(!strurl.startsWith(prefix))
            return false;
        strurl = strurl.substring(prefix.length());
        String[] values = strurl.split("&");

        String user="";
        String password="";
        String server="";
        for (String value : values) {
            try {
                if (value.startsWith("user:")) {
                    user = URLDecoder.decode(
                            value.substring("user:".length()), "UTF-8");
                } else if (value.startsWith("password:")) {
                    password = URLDecoder.decode(
                            value.substring("password:".length()), "UTF-8");
                } else if (value.startsWith("server:")) {
                    server = URLDecoder.decode(
                            value.substring("server:".length()), "UTF-8");
                }
            } catch (UnsupportedEncodingException ex) {
                Log.e("NextcloudLogin", "encoding not found (should not happen)", ex);
            }
        }
        if(!server.isEmpty() && !user.isEmpty() && !password.isEmpty()) {
            save(context, server, user, password);
            return true;
        } else {
            Log.e("NextcloudLogin", "Invalid response " + url);
            return false;
        }
    }

    public static void logout(final Context context) {
        OwnCloudClient client = getClient(context);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        prefs.edit().remove(nextCloudPassword).apply();
        if (client != null) {
            String baseURL = prefs.getString(nextCloudServer, null);
            deleteUserOnNextcloud(baseURL, client);
        }

    }

    // lösche Benutzer/App-Passwort vom Server
    private static void deleteUserOnNextcloud(final String baseURL, final OwnCloudClient client) {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                DeleteMethod deleteMethod = new DeleteMethod(baseURL + "/ocs/v2.php/core/apppassword");
                deleteMethod.addRequestHeader("OCS-APIREQUEST", "true");
                try {
                    client.executeMethod(deleteMethod);
                } catch (IOException e) {
                    Log.e("NextcloudLogin", "Could not delete user on server", e);
                }
                return null;
            }
        }.execute();
    }
    public static boolean isLoggedIn(Context context) {
        return getClient(context) != null;
    }

    private static int dp(Context context, int px) {
        return (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                px,
                context.getResources().getDisplayMetrics());
    }
}
