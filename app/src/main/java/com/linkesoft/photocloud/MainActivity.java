package com.linkesoft.photocloud;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import com.linkesoft.photocloud.databinding.ActivityMainBinding;
import com.owncloud.android.lib.common.OwnCloudClient;
import com.owncloud.android.lib.common.operations.OnRemoteOperationListener;
import com.owncloud.android.lib.common.operations.RemoteOperation;
import com.owncloud.android.lib.common.operations.RemoteOperationResult;
import com.owncloud.android.lib.resources.files.CreateFolderRemoteOperation;
import com.owncloud.android.lib.resources.files.DownloadFileRemoteOperation;
import com.owncloud.android.lib.resources.files.ReadFolderRemoteOperation;
import com.owncloud.android.lib.resources.files.UploadFileRemoteOperation;
import com.owncloud.android.lib.resources.files.model.RemoteFile;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

public class MainActivity extends AppCompatActivity implements ThumbnailsFragment.ThumbnailsFragmentInteractionListener {
    //private RecyclerView photosRecyclerView;
    //private PhotosRecyclerViewAdapter photosRecyclerViewAdapter;
    private ThumbnailsFragment photosFragment;
    private ActivityMainBinding binding;
    private static final int TAKE_PHOTO_REQUEST = 1;
    private File photoFile;

    public static File localPhotoDir;
    public static File remotePhotoDir;
    private static Set<String> uploadedFileNames = new HashSet<>();

    private Handler handler; // for result handling of background tasks
    private OwnCloudClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        localPhotoDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        remotePhotoDir = new File(localPhotoDir.getName());
        binding=ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        setSupportActionBar(binding.toolbar);

        photosFragment=(ThumbnailsFragment)getSupportFragmentManager().findFragmentById(R.id.photosFragment);

        binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePicture();
            }
        });

        boolean uploadedFileNames = MainActivity.uploadedFileNames.addAll(PreferenceManager.getDefaultSharedPreferences(this).getStringSet("uploadedFileNames", new HashSet<String>()));

        handler = new Handler();

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!NextcloudLogin.isLoggedIn(this))
            login();
        binding.root.post(new Runnable() {
            public void run() {
                // jetzt haben die Views eine Größe
                photosFragment.adapter.colunnWidth = binding.root.getWidth() / photosFragment.columnCount;
                photosFragment.adapter.refresh();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean loggedIn = NextcloudLogin.isLoggedIn(this);
        menu.findItem(R.id.action_login).setEnabled(!loggedIn);
        menu.findItem(R.id.action_sync).setEnabled(loggedIn);
        menu.findItem(R.id.action_logout).setEnabled(loggedIn);

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_login) {
            login();
            return true;
        } else if (id == R.id.action_logout) {
            logout();
            return true;
        } else if (id == R.id.action_sync) {
            sync();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void takePicture() {
        Intent takePhotoIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePhotoIntent.resolveActivity(getPackageManager()) != null) {
            String fileName = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.US).format(new Date());
            photoFile = new File(localPhotoDir, fileName + ".jpg");
            Uri photoURI = FileProvider.getUriForFile(this,
                    getPackageName() + ".provider",
                    photoFile);
            takePhotoIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            // siehe https://medium.com/@a1cooke/using-v4-support-library-fileprovider-and-camera-intent-a45f76879d61
            List<ResolveInfo> resolvedIntentActivities = getPackageManager().queryIntentActivities(takePhotoIntent, PackageManager.MATCH_DEFAULT_ONLY);
            for (ResolveInfo resolvedIntentInfo : resolvedIntentActivities) {
                String packageName = resolvedIntentInfo.activityInfo.packageName;
                grantUriPermission(packageName, photoURI, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
            }
            startActivityForResult(takePhotoIntent, TAKE_PHOTO_REQUEST);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == TAKE_PHOTO_REQUEST && resultCode == RESULT_OK) {
            if (data != null) {
                Bundle extras = data.getExtras();
                if (extras != null) {
                    Bitmap thumbnail = (Bitmap) extras.get("data");
                }
            }
            if (photoFile.exists()) {
                uploadPhoto(photoFile);
                photosFragment.adapter.refresh();
            }
        }
    }

    @Override
    public void onThumbnailSelected(File photoFile) {
        Log.d(getClass().getSimpleName(), "selected file " + photoFile);
        Intent i = new Intent(this, PhotoActivity.class);
        i.putExtra(PhotoActivity.EXTRA_PHOTOFILE, photoFile);
        startActivity(i);
    }

    // Nextcloud upload

    private void uploadPhoto(final File photoFile) {
        Log.d(getClass().getSimpleName(),"Uploading photo file "+photoFile);
        Long timestamp = photoFile.lastModified()/1000; // in s
        String remotePath = new File(remotePhotoDir,photoFile.getName()).getAbsolutePath();
        UploadFileRemoteOperation op = new UploadFileRemoteOperation(photoFile.getAbsolutePath(),remotePath,"image/jpg",timestamp.toString());
        op.execute(NextcloudLogin.getClient(this), new OnRemoteOperationListener() {
            @Override
            public void onRemoteOperationFinish(RemoteOperation caller, RemoteOperationResult result) {
                if(result.isSuccess()) {
                    markUploaded(getApplicationContext(),photoFile);
                    photosFragment.adapter.refresh();
                }
                if(result.isException())
                    Toast.makeText(getApplicationContext(),result.getException().getLocalizedMessage(),Toast.LENGTH_LONG).show();
            }
        },handler);
    }

    private void login() {
        NextcloudLogin.login(this, new NextcloudLogin.LoginHandler() {
            @Override
            public void loginSuccess() {
                // login erfolgreich, lege Pictures Verzeichnis an (falls es noch nicht existiert)
                createPicturesDirectory();
            }
        });
    }
    private void logout() {
        NextcloudLogin.logout(this);
    }
    private void createPicturesDirectory() {
        CreateFolderRemoteOperation op = new CreateFolderRemoteOperation(remotePhotoDir.getAbsolutePath(),true);
        op.execute(NextcloudLogin.getClient(this), new OnRemoteOperationListener() {
            @Override
            public void onRemoteOperationFinish(RemoteOperation caller, RemoteOperationResult result) {
                if(result.isException())
                    Toast.makeText(getApplicationContext(),result.getException().getLocalizedMessage(),Toast.LENGTH_LONG).show();
            }
        },handler);
    }
    private int countSyncOps;
    private void sync() {
        binding.progress.setVisibility(View.VISIBLE);
        uploadedFileNames.clear();
        countSyncOps=0;
        final Set<String> localFileNames = new HashSet<>();
        File[] localFiles = localPhotoDir.listFiles();
        if(localFiles != null) {
            for (File file : localFiles) {
                if(!file.isDirectory())
                    localFileNames.add(file.getName());
            }
        }
        final OwnCloudClient client = NextcloudLogin.getClient(this);
        ReadFolderRemoteOperation op = new ReadFolderRemoteOperation(remotePhotoDir.getAbsolutePath());
        countSyncOps++;
        op.execute(client, new OnRemoteOperationListener() {
            @Override
            public void onRemoteOperationFinish(RemoteOperation caller, RemoteOperationResult result) {
                countSyncOps--;
                if(result.isSuccess()) {
                    List<RemoteFile> files = new ArrayList<>();
                    for(Object obj: result.getData()) {
                        final RemoteFile remoteFile = (RemoteFile) obj;
                        // ignoriere Verzeichnisse
                        if (remoteFile.getMimeType().equals("DIR"))
                            continue;
                        final String remoteFileName = new File(remoteFile.getRemotePath()).getName();
                        Log.d(getClass().getSimpleName(),"Remote file "+remoteFileName);
                        if(localFileNames.contains(remoteFileName)) {
                            // schon vorhanden
                            markUploaded(getApplicationContext(),new File(remoteFileName));
                            photosFragment.adapter.refresh();
                            localFileNames.remove(remoteFileName);
                        } else {
                            DownloadFileRemoteOperation op = new DownloadFileRemoteOperation(remoteFile.getRemotePath(), localPhotoDir.getParent()); // final target path is localDir + remotePath
                            countSyncOps++;
                            op.execute(client, new OnRemoteOperationListener() {
                                @Override
                                public void onRemoteOperationFinish(RemoteOperation caller, RemoteOperationResult result) {
                                    countSyncOps--;
                                    if(result.isSuccess()) {
                                        // setze letztes Änderungsdatum
                                        new File(localPhotoDir, remoteFileName).setLastModified(remoteFile.getModifiedTimestamp());
                                        markUploaded(getApplicationContext(),new File(remoteFileName));
                                        photosFragment.adapter.refresh();
                                    } else if(result.isException()) {
                                        // download file exception
                                        Toast.makeText(getApplicationContext(),result.getException().getLocalizedMessage(),Toast.LENGTH_LONG).show();
                                    }
                                    binding.progress.setVisibility(countSyncOps>0 ? View.VISIBLE : View.GONE);
                                }
                            },handler);
                        }
                        binding.progress.setVisibility(countSyncOps>0 ? View.VISIBLE : View.GONE);
                    } // for remote files
                    // alle files, die jetzt noch in localFileNames sind, wurden noch nicht hochgeladen
                    for(final String localFileName: localFileNames) {
                        final File localFile = new File(localPhotoDir, localFileName);
                        Long timestamp = localFile.lastModified() / 1000; // in s
                        String remotePath = new File(remotePhotoDir, localFile.getName()).getAbsolutePath();
                        UploadFileRemoteOperation op = new UploadFileRemoteOperation(localFile.getAbsolutePath(), remotePath, "image/jpg", timestamp.toString());
                        countSyncOps++;
                        op.execute(client, new OnRemoteOperationListener() {
                            @Override
                            public void onRemoteOperationFinish(RemoteOperation caller, RemoteOperationResult result) {
                                countSyncOps--;
                                if (result.isSuccess()) {
                                    markUploaded(getApplicationContext(), localFile);
                                    photosFragment.adapter.refresh();
                                } else if(result.isException()) {
                                    // upload file exception
                                    Toast.makeText(getApplicationContext(),result.getException().getLocalizedMessage(),Toast.LENGTH_LONG).show();
                                }
                                binding.progress.setVisibility(countSyncOps>0 ? View.VISIBLE : View.GONE);
                            }
                        }, handler);
                    } // for local files
                } else if(result.isException()) {
                    // list files exception
                    Toast.makeText(getApplicationContext(),result.getException().getLocalizedMessage(),Toast.LENGTH_LONG).show();
                }
                binding.progress.setVisibility(countSyncOps>0 ? View.VISIBLE : View.GONE);
            }
        },handler);
    }
    static void markUploaded(Context context, File photoFile) {
        uploadedFileNames.add(photoFile.getName());
        PreferenceManager.getDefaultSharedPreferences(context).edit().putStringSet("uploadedFileNames",uploadedFileNames).apply();
    }
    static boolean isUploaded(File photoFile) {
        return uploadedFileNames.contains(photoFile.getName());
    }


}
